/*Выборки:*/
/*1. Вывести таблицу распределения мест в соревновании 'открытый чемпионат' в городе 'Васюки' по 'шахматам' в 2000 г.*/
SELECT DISTINCT [PlaceOfSportman], [FirstName], [LastName]
FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) 
INNER JOIN [CompetitionResult] ON ([Result].ID = [CompetitionResult].ResultID) INNER JOIN [Competition] ON ([CompetitionResult].CompetitionID = [Competition].ID)
INNER JOIN [CompetitionCity] ON ([Competition].ID = [CompetitionCity].CompetitionID) INNER JOIN [City] ON ([CompetitionCity].CityID = [City].ID)
WHERE ([Competition].Name = 'Открытый чемпионат' AND [Sport].Name = 'Chessmate' AND City.Name = 'Васюки')
ORDER BY [PlaceOfSportman];

/*2. Определить спортсменов, которые имеют как олимпийский, так и мировой рекорд.*/
SELECT DISTINCT [FirstName], [LastName]
FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) 
WHERE ([Sport].WorldRecord = [Result].ShownResult)
INTERSECT
SELECT DISTINCT [FirstName], [LastName]
FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID)
WHERE ([Sport].OlympicRecord = [Result].ShownResult);

/*3. Вывести список спортсменов, участвующих более чем в 8 соревнованиях в году в порядке возрастания среднего места на этих соревнованиях.*/
WITH [MyTable1] AS
(	
	SELECT DISTINCT [FirstName], [LastName], [PlaceOfSportman], [Sportman].ID as [SportmanID], [Sport].ID as [SportID], [Competition].ID as [CompetitionID], YEAR([CompetitionDate]) AS [Year]
	FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) 
	INNER JOIN [CompetitionResult] ON ([Result].ID = [CompetitionResult].ResultID) INNER JOIN [Competition] ON ([CompetitionResult].CompetitionID = [Competition].ID)
),
[MyTable2] AS
(
	SELECT DISTINCT [SportmanID], [Year], COUNT([CompetitionID]) AS [NumOfCompetitions]
	FROM [MyTable1] 
	GROUP BY [SportmanID], [Year]
),
[MyTable3] AS
(
	SELECT DISTINCT [MyTable2].[SportmanID], CAST(AVG([MyTable1].[PlaceOfSportman]) AS NUMERIC(2, 0)) AS [AVG PlaceOfSportman]
	FROM [MyTable2] INNER JOIN [MyTable1] ON ([MyTable2].SportmanID = [MyTable1].SportmanID)
	WHERE ([NumOfCompetitions] >= 8)
	GROUP BY [MyTable2].[SportmanID]
)

SELECT DISTINCT [AVG PlaceOfSportman], [FirstName], [LastName]
FROM [Mytable3] INNER JOIN [Sportman] ON ([MyTable3].SportmanID = [Sportman].ID)
ORDER BY [AVG PlaceOfSportman];

/*4. Определить наилучший показатель спортсмена 'Караваев' в виде спорта 'тараканьи бега' и разницу с мировым и олимпийским рекордами.*/
WITH [MyTable] AS
(
	SELECT DISTINCT [ShownResult], [FirstName], [LastName], [WorldRecord], [OlympicRecord]
	FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) 
	INNER JOIN [CompetitionResult] ON ([Result].ID = [CompetitionResult].ResultID) INNER JOIN [Competition] ON ([CompetitionResult].CompetitionID = [Competition].ID)
	INNER JOIN [CompetitionCity] ON ([Competition].ID = [CompetitionCity].CompetitionID) INNER JOIN [City] ON ([CompetitionCity].CityID = [City].ID)
	WHERE ([Sportman].LastName = 'Карасев' AND [Sport].Name = 'Speed skating (500 metres)') /*примечание: 'тараканьи бега' я принял за 'Speed skating (500 metres)'*/
),
[MyTable2] AS
(
SELECT MAX([ShownResult]) As MaxResult
FROM [MyTable]
)

SELECT DISTINCT [FirstName], [LastName], [MaxResult], ([MaxResult]-[WorldRecord]) AS deltaWorldRecord, ([MaxResult]-[OlympicRecord]) AS deltaOlympicRecord
FROM [MyTable], [MyTable2];

/*5. Определить для каждой страны общее количество побед спортменами, занявших 1 место в любом соревновании и в любом виде спорта.*/
WITH [MyTable1] AS
(
	SELECT DISTINCT [PlaceOfSportman], [Sportman].ID, [FirstName], [LastName], [Sport].Name AS [SportName], [Competition].Name AS [CompetitionName], [Country].Name
	AS [Country], [Result].ID AS [ResultID]
	FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) 
	INNER JOIN [CompetitionResult] ON ([Result].ID = [CompetitionResult].ResultID) INNER JOIN [Competition] ON ([CompetitionResult].CompetitionID = [Competition].ID)
	INNER JOIN [CompetitionCity] ON ([Competition].ID = [CompetitionCity].CompetitionID) INNER JOIN [City] ON ([CompetitionCity].CityID = [City].ID)
	INNER JOIN [Country] ON ([Sportman].CountryID = [Country].ID)
),
[MyTable2] AS
(
	SELECT DISTINCT COUNT(ResultID) AS CountOFResult, [Country]
	FROM [MyTable1]
	GROUP BY [Country]
	HAVING (MIN([PlaceOfSportman]) <> 1)
),
[MyTable3] AS
(
	SELECT *
	From [MyTable1]
	WHERE ([PlaceOfSportman] = 1)
),
[MyTable4] AS
(
	SELECT COUNT([ResultID]) AS [Winners], [Country]
	From [MyTable3]
	GROUP BY [Country]
	UNION
	SELECT 0 AS [Winners], [Country]
	FROM [MyTable2]
	UNION
	SELECT 0 AS [Winners], [Name] AS [Country]
	FROM
	(
		SELECT [Country].Name
		FROM [Country]
		EXCEPT
		SELECT [Country].Name
		FROM [Sportman] INNER JOIN [Country] ON ([Sportman].CountryID = [Country].ID)
	) AS [CountriesThatDontHaveSportmans]
)

SELECT DISTINCT [Country], [Winners]
FROM [MyTable4]
ORDER BY Winners DESC, Country;

/*6. Определить наилучший результат спортменов, участвовавших в соревнованиях, для каждого вида спорта.*/
WITH [MyTable1] AS
(
	SELECT DISTINCT [ShownResult], [Sportman].ID, [FirstName], [LastName], [Sport].Name AS [SportName], [Competition].Name AS [CompetitionName], [Country].Name
	AS [Country], [Result].ID AS [ResultID]
	FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) 
	INNER JOIN [CompetitionResult] ON ([Result].ID = [CompetitionResult].ResultID) INNER JOIN [Competition] ON ([CompetitionResult].CompetitionID = [Competition].ID)
	INNER JOIN [CompetitionCity] ON ([Competition].ID = [CompetitionCity].CompetitionID) INNER JOIN [City] ON ([CompetitionCity].CityID = [City].ID)
	INNER JOIN [Country] ON ([Sportman].CountryID = [Country].ID)
)

SELECT DISTINCT [SportName], MAX([ShownResult]) AS MaxResult
FROM [MyTable1]
GROUP BY [SportName];

/*7. Вывести города, которые принимали у себя спортменов в соревнованиях из той же страны, что и город.*/
WITH [MyTable1] AS
(
	SELECT DISTINCT [City].Name AS [City], [Country].Name AS [Country], [Country].ID AS [CountryID], [Sportman].CountryID AS [SportmanCountryID] 
	FROM [Sportman] INNER JOIN [Result] ON ([Sportman].ID = [Result].SportmanID) INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) 
	INNER JOIN [CompetitionResult] ON ([Result].ID = [CompetitionResult].ResultID) INNER JOIN [Competition] ON ([CompetitionResult].CompetitionID = [Competition].ID)
	INNER JOIN [CompetitionCity] ON ([Competition].ID = [CompetitionCity].CompetitionID) INNER JOIN [City] ON ([CompetitionCity].CityID = [City].ID)
	INNER JOIN [Country] ON ([City].CountryID = [Country].ID)
)

SELECT DISTINCT [City], [Country]
FROM [MyTable1]
WHERE ([CountryID] = [SportmanCountryID]);




/*Запросы:*/
/*1. Удалить результаты, набравшие в итоге менее 4 очков.*/
ALTER TABLE [CompetitionResult]
	DROP CONSTRAINT [FK_CompetitionResult_ResultID]

DELETE FROM [CompetitionResult]
	WHERE [CompetitionResult].ResultID IN
		(
			SELECT DISTINCT [ID]
			FROM [Result]
			WHERE ([ShownResult] < 30)
		)

DELETE FROM [Result]
	WHERE [Result].ID IN
		(
			SELECT DISTINCT [ID]
			FROM [Result]
			WHERE ([ShownResult] < 30)
		)

ALTER TABLE [CompetitionResult]
	ADD CONSTRAINT [FK_CompetitionResult_ResultID] FOREIGN KEY ([ResultID])
		REFERENCES [Result]([ID]);

/*2. Заменить все имена и фамилии спортменов на инициалы.*/
SELECT [ID], [FirstName], [LastName] FROM [Sportman];
UPDATE [Sportman] SET
[FirstName] = SUBSTRING([FirstName], 1, 1),
[LastName] = SUBSTRING([LastName], 1, 1);
SELECT [ID], [FirstName], [LastName] FROM [Sportman];

/*3. Попытаться удалить спортменов из России.*/
DELETE FROM [Sportman]
	WHERE [Sportman].ID IN
		(
			SELECT DISTINCT [Sportman].[ID]
			FROM [Sportman] INNER JOIN [Country] ON ([Sportman].CountryID = [Country].ID)
			WHERE ([Country].Name = 'Russia')
		)

/*4. Попытаться выдавать за спортмена трехлетнего ребенка.*/
UPDATE [Sportman] SET [BirthDate] = convert(datetime, '2013-05-25', 102) WHERE [Sportman].ID = 1