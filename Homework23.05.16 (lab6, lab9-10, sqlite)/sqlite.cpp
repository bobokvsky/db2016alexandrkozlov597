#include <iostream>
#include <sqlite3.h>
#include <string>
#include <vector>
#include <map>
#include <ctime>
#include <cstdlib>
int Random(int min, int max) {
	return rand() % (max - min + 1) + min;
}

using namespace std;

map<int, vector<pair<int, string>>> graph;
void DFS_print(int vertex, string name, int tabs = 0) {
	if (name != "") {
		for (int i = 0; i < tabs; i++)
			cout << "    ";
		cout << name << endl;
	}
	vector<pair<int, string>> vp = graph[vertex];
	for (int i = 0; i < vp.size(); i++)
		DFS_print(vp[i].first, vp[i].second, tabs + 1);
}

void DFS_find(int vertex, string name, int count, int max) {
	if (count == max){
		cout << name << endl;
		return;
	}
	vector<pair<int, string>> vp = graph[vertex];
	for (int i = 0; i < vp.size(); i++)
		DFS_find(vp[i].first, vp[i].second, count + 1, max);
}


static int callback_printtable(void* NotUsed, int argc, char** argv, char** azColName) {
	string id_s = argv[0];
	string parent_id_s = (argv[1] ? argv[1] : "0");
	int id = stoi(id_s);
	int parent_id = stoi(parent_id_s);
	graph[parent_id].push_back(make_pair(id, id_s));
	return 0;
}

int main(int argc, char* argv[]) {
	srand(time(NULL));
	sqlite3* db;
	char* zErrMsg = 0;
	int  rc;
	string sql;

	/* Open database */
	rc = sqlite3_open("test.db", &db);
	if (rc) {
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		exit(0);
	} else
		cout << "Opened database successfully" << endl;


	/* Create SQL statement */
	sql = "CREATE TABLE [Table]"  \
	      "(" \
	      "id int," \
	      "parent_id int" \
	      ");";

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql.c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	} else
		cout << "[Table] created successfully" << endl;

	/* Create merged SQL statement */
	string arg3 = (argv[3]) ? argv[3] : "30";
	int count_data = stoi(arg3);
	sql = "INSERT INTO [Table] VALUES (1, NULL), ";
	for (int i = 2; i < count_data; i++) {
		int parent_id = Random(0, i - 1);
		if (parent_id == 0)
			sql = sql + "(" + to_string(i) + ", NULL)";
		else
			sql = sql + "(" + to_string(i) + ", " + to_string(parent_id) + ")";
		if (i < count_data - 1)
			sql = sql + ", ";
		else
			sql = sql + ";";
	}

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql.c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	} else
		cout << "[Table] has updated successfully" << endl;

	/* Create SQL statement */
	sql = "SELECT * FROM [Table];";

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql.c_str(), callback_printtable, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	} else {
		string arg1 = (argv[1]) ? argv[1] : "0";
		string arg2 = (argv[2]) ? argv[2] : "3";
		int id = stoi(arg1);
		int max = stoi(arg2);
		cout << "Tree:" << endl << endl;
		DFS_print(0, "", -1);
		cout << endl << endl;
		cout << "Result:" << endl;
		DFS_find(id, "", 0, max);
		cout << endl << endl;
	}



	/* Create SQL statement */
	sql = "DROP TABLE [Table];";

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql.c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	} else
		cout << "[Table] dropped successfully" << endl;



	sqlite3_close(db);
	return 0;
}