/*Будем работать лишь с одной таблицей: [Sportman]. Выполнять построчно и по очереди с Isolations1. Для восстановления данных запускать DropAllTables->CreateTables.*/

/*READ UNCOMMITTED*/
/*1. грязное чтение.*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED BEGIN TRANSACTION
UPDATE [Sportman] SET [BirthDate] = convert(datetime, '1974-02-24', 102) WHERE ID = 1
ROLLBACK
/*Итого: грязное чтение допускается.*/

/*1. потерянные изменения.*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED BEGIN TRANSACTION
UPDATE [Sportman] SET [CountryID]=5 WHERE [ID]=6
COMMIT
/*Итого: потерянные изменения НЕ допускается.*/

/*READ COMMITTED*/
/*2. грязное чтение.*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED BEGIN TRANSACTION
UPDATE [Sportman] SET [BirthDate] = convert(datetime, '1974-02-24', 102) WHERE ID = 1
ROLLBACK
/*Итого: грязное чтение НЕ допускается.*/

/*2. неповторяющиеся чтения.*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED BEGIN TRANSACTION
UPDATE [Sportman] SET [FirstName] = 'Petr' WHERE ID = 11
COMMIT
/*Итого: неповторяющиеся чтения допускаются.*/

/*REPEATABLE READ*/
/*3. неповторяющиеся чтения.*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ BEGIN TRANSACTION
UPDATE [Sportman] SET [FirstName] = 'Petr' WHERE ID = 11
COMMIT
/*Итого: неповторяющиеся чтения НЕ допускаются.*/

/*3. фантомы.*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ BEGIN TRANSACTION
SET IDENTITY_INSERT [Sportman] ON /*выполнять за 1 действие все последующие 3 строки*/
INSERT [Sportman] ([ID], [FirstName], [LastName], [BirthDate], [CountryID], [NumberOfVictories]) VALUES (21, 'Nekto', 'Nechto', convert(datetime, '1991-11-17', 102), 16, 6) /*выполнять за 1 действие*/
SET IDENTITY_INSERT [Sportman] ON /*выполнять за 1 действие*/
COMMIT /*выполнять за 1 действие*/
/*Итого: фантомы допускаются.*/

/*SERIALIZABLE*/
/*4. фантомы.*/
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE BEGIN TRANSACTION
SET IDENTITY_INSERT [Sportman] ON /*выполнять за 1 действие все последующие 3 строки*/
INSERT [Sportman] ([ID], [FirstName], [LastName], [BirthDate], [CountryID], [NumberOfVictories]) VALUES (21, 'Nekto', 'Nechto', convert(datetime, '1991-11-17', 102), 16, 6) /*выполнять за 1 действие*/
SET IDENTITY_INSERT [Sportman] ON /*выполнять за 1 действие*/
COMMIT
/*Итого: фантомы НЕ допускаются.*/