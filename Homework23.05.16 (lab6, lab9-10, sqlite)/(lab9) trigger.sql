/*исправление мирового рекорда при появлении соответствующего результата.*/
/*проверка и дроп триггера находится в файле (lab9) trigger (checking).sql*/
CREATE TRIGGER FixRecord
	ON [Result] AFTER INSERT AS
	IF EXISTS(
				SELECT [ShownResult]
				FROM [Result] INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) INNER JOIN 
				(
					SELECT MAX([Result].ID) as MaxID
					FROM [Result] INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID)
					WHERE [ShownResult] > [WorldRecord]
				) AS [MyTable] ON ([Result].ID = [MyTable].MaxID)
			)
	BEGIN
		WITH [MaxID] AS
		(
			SELECT MAX([Result].ID) AS ID
			FROM [Result] INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID)
			WHERE [ShownResult] > [WorldRecord]
		),
		[MaxResult] AS
		(
			SELECT [ShownResult], [Sport].ID AS SportID
			FROM [Result] INNER JOIN [Sport] ON ([Result].SportID = [Sport].ID) INNER JOIN [MaxID] ON ([Result].ID = [MaxID].ID)
		)

		UPDATE [Sport] SET [Sport].WorldRecord=(SELECT [MaxResult].ShownResult FROM [MaxResult]) WHERE [ID]=(SELECT [MaxResult].SportID FROM [MaxResult])
	END