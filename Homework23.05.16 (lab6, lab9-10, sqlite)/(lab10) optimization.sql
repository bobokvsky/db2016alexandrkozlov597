SET STATISTICS TIME ON
SET STATISTICS IO ON

/*Оценим производительность:*/
SELECT [FirstName], [LastName]
FROM [SportmanLarge]
WHERE [FirstName] = 'John' AND [LastName] = 'Smith'
/*Итого: CPU time = 329 ms,  elapsed time = 3395 ms.*/
SELECT [FirstName], [LastName], [CountryLarge].Name
FROM [SportmanLarge] INNER JOIN [CountryLarge] ON [SportmanLarge].CountryID = [CountryLarge].ID
WHERE [FirstName] = 'John' AND [LastName] = 'Smith'
/*An error occurred while executing batch. Error message is: Выдано исключение типа "System.OutOfMemoryException".
Не могу ничего с этим поделать. По идее, должно быть меньше*/



/*Очищаем кэш так:
CHECKPOINT
GO
DBCC DROPCLEANBUFFERS
GO*/

/*Создаем индексы*/
CREATE NONCLUSTERED INDEX [IX_FirstNameLastName] ON [SportmanLargeIndex]
([FirstName] ASC, [LastName] ASC);
CREATE CLUSTERED INDEX [IX_ID_Name] ON [CountryLargeIndex]
([ID] ASC);

/*Оценим производительность к индексу:*/
SELECT [FirstName], [LastName]
FROM [SportmanLargeIndex]
WHERE [FirstName] = 'John' AND [LastName] = 'Smith'
/*Итого: CPU time = 47 ms,  elapsed time = 864 ms.*/
SELECT [FirstName], [LastName], [CountryLargeIndex].Name
FROM [SportmanLargeIndex] INNER JOIN [CountryLargeIndex] ON [SportmanLargeIndex].CountryID = [CountryLargeIndex].ID
WHERE [FirstName] = 'John' AND [LastName] = 'Smith'
/*An error occurred while executing batch. Error message is: Выдано исключение типа "System.OutOfMemoryException".
Не могу ничего с этим поделать. По идее, должно быть меньше*/


/*Первое: CPU time = 219 ms,  elapsed time = 950 ms.*/
/*Второе: CPU time = 32 ms,  elapsed time = 844 ms.*/

/*С JOIN - не знаю, OutOfMemoryException же!*/