/*исправление мирового рекорда при появлении соответствующего результата.*/
/*создание триггера находится в файле (lab9) trigger.sql*/
/*как обычно, удаление и восстановление данных через DropAllTables->CreateTables.*/

/*INSERT [Sport] ([ID], [Name], [UnitOfMeasurement], [WorldRecord], [OlympicRecord]) VALUES (5, 'Swimming (50 metres)', 'Second', 44, 33)*/

SELECT [Name], [WorldRecord]
FROM [Sport] WHERE [Name] = 'Swimming (50 metres)'

SET IDENTITY_INSERT [Result] ON
INSERT [Result] ([ID], [SportID], [SportmanID], [PlaceOfSportman], [ShownResult]) VALUES (501,5,4,1,100)
SET IDENTITY_INSERT [Result] OFF

SELECT [Name], [WorldRecord]
FROM [Sport] WHERE [Name] = 'Swimming (50 metres)'