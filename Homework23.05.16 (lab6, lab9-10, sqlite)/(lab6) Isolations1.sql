/*Будем работать лишь с одной таблицей: [Sportman]. Выполнять построчно и по очереди с Isolations2. Для восстановления данных запускать DropAllTables->CreateTables.*/

/*READ UNCOMMITTED*/
/*1. грязное чтение.*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED BEGIN TRANSACTION
SELECT [BirthDate] FROM [Sportman] WHERE [ID] = 1
SELECT [BirthDate] FROM [Sportman] WHERE [ID] = 1
SELECT [BirthDate] FROM [Sportman] WHERE [ID] = 1
COMMIT
/*Итого: грязное чтение допускается.*/

/*1. потерянные изменения.*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED BEGIN TRANSACTION
UPDATE [Sportman] SET [CountryID]=7 WHERE [ID]=6
COMMIT
/*Итого: потерянные изменения НЕ допускается.*/

/*READ COMMITTED*/
/*2. грязные чтения.*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED BEGIN TRANSACTION
SELECT [BirthDate] FROM [Sportman] WHERE [ID] = 1
SELECT [BirthDate] FROM [Sportman] WHERE [ID] = 1
SELECT [BirthDate] FROM [Sportman] WHERE [ID] = 1
COMMIT
/*Итого: грязное чтение НЕ допускается.*/

/*2. неповторяющиеся чтения.*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED BEGIN TRANSACTION
SELECT * FROM [Sportman] WHERE [ID] = 11
SELECT * FROM [Sportman] WHERE [ID] = 11
COMMIT
/*Итого: неповторяющиеся чтения допускаются.*/

/*REPEATABLE READ*/
/*3. неповторяющиеся чтения.*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ BEGIN TRANSACTION
SELECT * FROM [Sportman] WHERE [ID] = 11
SELECT * FROM [Sportman] WHERE [ID] = 11
COMMIT
/*Итого: неповторяющиеся чтения НЕ допускаются.*/

/*3. фантомы.*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ BEGIN TRANSACTION
SELECT * FROM [Sportman] WHERE [CountryID] > 15
SELECT * FROM [Sportman] WHERE [CountryID] > 15
COMMIT
/*Итого: фантомы допускаются.*/

/*SERIALIZABLE*/
/*4. фантомы.*/
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE BEGIN TRANSACTION
SELECT * FROM [Sportman] WHERE [CountryID] > 15
SELECT * FROM [Sportman] WHERE [CountryID] > 15
COMMIT
/*Итого: фантомы НЕ допускаются.*/