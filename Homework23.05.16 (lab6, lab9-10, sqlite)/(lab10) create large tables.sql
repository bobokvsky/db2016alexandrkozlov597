/*Для удаления юзаем 
DROP TABLE [SportmanLarge]
DROP TABLE [SportmanLargeIndex]
DROP TABLE [CountryLarge]
DROP TABLE [CountryLargeIndex]
*/
/*Сначала выполняем первые четыре строки, после чего - оставшиеся.*/

SELECT * INTO [SportmanLarge] FROM [Sportman]
SELECT * INTO [SportmanLargeIndex] FROM [Sportman]
SELECT * INTO [CountryLarge] FROM [Country]
SELECT * INTO [CountryLargeIndex] FROM [Country]

SET IDENTITY_INSERT [SportmanLarge] ON
INSERT INTO [SportmanLarge] ([ID], [FirstName], [LastName], [BirthDate], [CountryID], [NumberOfVictories])
SELECT [ID], [FirstName], [LastName], [BirthDate], [CountryID], [NumberOfVictories]
FROM [Sportman]
SET IDENTITY_INSERT [SportmanLarge] OFF

SET IDENTITY_INSERT [SportmanLargeIndex] ON
INSERT INTO [SportmanLargeIndex] ([ID], [FirstName], [LastName], [BirthDate], [CountryID], [NumberOfVictories])
SELECT [ID], [FirstName], [LastName], [BirthDate], [CountryID], [NumberOfVictories]
FROM [Sportman]
SET IDENTITY_INSERT [SportmanLargeIndex] OFF

SET IDENTITY_INSERT [CountryLarge] ON
INSERT INTO [CountryLarge] ([ID], [Name])
SELECT [ID], [Name]
FROM [Country]
SET IDENTITY_INSERT [CountryLarge] OFF

SET IDENTITY_INSERT [CountryLargeIndex] ON
INSERT INTO [CountryLargeIndex] ([ID], [Name])
SELECT [ID], [Name]
FROM [Country]
SET IDENTITY_INSERT [CountryLargeIndex] OFF

GO 100000