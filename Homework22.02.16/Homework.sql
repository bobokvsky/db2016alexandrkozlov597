SELECT DISTINCT model, speed, hd
FROM PC 
WHERE price < 500 
ORDER BY 2 DESC

SELECT DISTINCT maker
from product
WHERE type = 'Printer'

SELECT model, ram, screen
from Laptop
WHERE price > 1000

SELECT code, model, color, type, price
FROM Printer
WHERE color = 'y'

SELECT model, speed, hd
FROM PC
WHERE (cd = '12x' OR cd = '24x') AND price < 600

SELECT DISTINCT Product.maker, Laptop.speed 
FROM Laptop INNER JOIN Product
ON Product.model = Laptop.model AND Laptop.hd >= 10

SELECT DISTINCT PC.model, PC.price FROM
Product INNER JOIN PC ON (Product.maker = 'B' AND Product.model = PC.model)
UNION
SELECT DISTINCT Laptop.model, Laptop.price FROM
Product INNER JOIN Laptop ON (Product.maker = 'B' AND Product.model = Laptop.model)
UNION
SELECT DISTINCT Printer.model, Printer.price FROM
Product INNER JOIN Printer ON (Product.maker = 'B' AND Product.model = Printer.model)

SELECT p.maker
FROM Product p WHERE p.type = 'PC'
EXCEPT
SELECT p.maker
FROM Product p WHERE p.type = 'Laptop'

SELECT DISTINCT maker
FROM Product p INNER JOIN PC ON p.model = PC.model
WHERE PC.speed >= 450

SELECT model, price
FROM Printer l
WHERE price = (SELECT MAX(price) FROM Printer)

SELECT AVG(speed)
FROM PC

SELECT AVG(speed)
FROM laptop
WHERE price > 1000

SELECT AVG(speed)
FROM PC INNER JOIN Product ON PC.model = Product.model AND Product.maker = 'A'

SELECT DISTINCT maker, type FROM Product WHERE maker in 
(SELECT maker from Product
GROUP BY maker HAVING COUNT(model) > 1 AND COUNT(DISTINCT type) = 1)

SELECT hd FROM PC
GROUP BY hd
HAVING COUNT(hd) >= 2