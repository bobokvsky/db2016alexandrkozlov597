CREATE TABLE mytable
(
	 id int,
	 parent_id int
)

INSERT INTO mytable VALUES
	(1, NULL),
	(2, 1),
	(3, 1),
	(4, 3),
	(5, 2),
	(6, 4),
	(7, 4),
	(8, 7),
	(9, 8),
	(10, 8),
	(11, 10),
	(12, 10);

WITH table2 AS
	(
	SELECT id AS Ierarhy, id, parent_id	
	FROM mytable
	UNION ALL
	SELECT table2.Ierarhy, mytable.id, mytable.parent_id
	FROM mytable INNER JOIN table2
		ON mytable.id = table2.parent_id
	),

	table3 AS (
	SELECT DISTINCT TOP 100 *
	FROM table2
	WHERE (Ierarhy <> id) AND (Ierarhy = 12) /*ЗДЕСЬ УКАЗЫВАЕМ ЗАДАННЫЙ id*/
	ORDER BY IErarhy, id DESC
	)

SELECT id
FROM table3

DROP TABLE mytable