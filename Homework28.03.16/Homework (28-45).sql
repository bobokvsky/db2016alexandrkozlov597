SELECT CAST(1.0 * 
CASE
  WHEN (SELECT Sum(B_VOL) FROM utB) IS NULL 
    THEN 0 
  ELSE (SELECT Sum(B_VOL) FROM utB) 
END
/
(SELECT count(*) FROM utQ) 
AS NUMERIC(6,2))

SELECT ISNULL(Income_o.point, Outcome_o.point), ISNULL(Income_o.date, Outcome_o.date), Income_o.inc, Outcome_o.out
FROM Income_o FULL JOIN Outcome_o ON ((Income_o.date = Outcome_o.date) AND (Income_o.point = Outcome_o.point))

WITH MyTable AS 
(SELECT point, date, NULL AS Outcome, SUM(inc) AS Income
FROM Income
Group BY point, date
UNION
SELECT point, date, SUM(out), NULL
FROM Outcome
Group BY point, date)
SELECT point, date, SUM(Outcome) AS Outcome, SUM(Income) AS Income
FROM MyTable
GROUP BY date, point

SELECT class, country
FROM Classes
WHERE bore >= 16

With MY AS (
SELECT Ships.name, Ships.class
FROM Ships
UNION
SELECT Classes.class, Classes.class
FROM Classes INNER JOIN Outcomes ON (Outcomes.ship = Classes.class)
)
SELECT Classes.country, CAST(AVG(Classes.bore*Classes.bore*Classes.bore/2) AS NUMERIC(6,2))
FROM MY INNER JOIN Classes ON Classes.class = MY.class
GROUP BY Classes.country

SELECT ship
FROM Outcomes
WHERE (battle = 'North Atlantic') and (result = 'sunk')

SELECT Ships.name
FROM Ships INNER JOIN Classes ON Ships.class = Classes.class
WHERE Ships.launched >= 1922 AND Classes.displacement >= 35000 AND Classes.type = 'bb'

SELECT model, type
FROM Product
WHERE (model NOT LIKE '%[^0-9]%') or (model NOT LIKE '%[^a-zA-Z]%')

SELECT DISTINCT Classes.class
FROM Classes INNER JOIN Ships ON (classes.class = ships.name)
UNION
SELECT DISTINCT Classes.class
FROM Classes INNER JOIN Outcomes ON (classes.class = Outcomes.ship)

WITH MY AS (
SELECT Classes.*, Ships.name
FROM Classes INNER JOIN Ships ON (Classes.class = Ships.class)
UNION
SELECT Classes.*, Outcomes.ship
FROM Classes INNER JOIN Outcomes ON (Classes.class = Outcomes.ship)
)
SELECT MY.class
FROM MY
GROUP BY class
HAVING count(*) = 1

SELECT DISTINCT country
FROM Classes
WHERE type = 'bb'
INTERSECT
SELECT DISTINCT country
FROM Classes
WHERE type = 'bc'

SELECT DISTINCT table1.ship
FROM Outcomes AS table1 INNER JOIN Battles AS table2 ON (table1.battle = table2.name)
WHERE result = 'damaged' AND EXISTS
(
SELECT Outcomes.ship
FROM Outcomes INNER JOIN Battles ON (Battles.name = Outcomes.battle)
WHERE (table1.ship = Outcomes.ship) AND (Battles.date > table2.date)
)

SELECT Classes.class, Ships.name, Classes.country
FROM Ships INNER JOIN Classes ON (Classes.class = Ships.class)
WHERE Classes.numGuns >= 10

WITH My1 AS (
SELECT CAST(model AS CHAR(25)) AS model,
CAST(speed AS CHAR(25)) AS speed,
CAST(ram AS CHAR(25)) AS ram,
CAST(hd AS CHAR(25)) AS hd,
CAST(cd AS CHAR(25)) AS cd,
CAST(price AS CHAR(25)) AS price
FROM PC
WHERE code = (SELECT MAX(code) FROM PC)
)
SELECT char, value FROM My1
UNPIVOT
(value FOR char IN (model, speed, ram, hd, cd, price)) AS unpvt

SELECT ship, battle
FROM Outcomes
WHERE result = 'sunk'

SELECT Battles.name
FROM Battles
WHERE DATEPART(yyyy, Battles.date) NOT IN (SELECT launched FROM Ships
WHERE launched IS NOT NULL)

SELECT DISTINCT name
FROM Ships
WHERE name LIKE 'R%'
UNION
SELECT DISTINCT ship
FROM Outcomes
WHERE ship LIKE 'R%'

SELECT DISTINCT name
FROM Ships
WHERE name LIKE '% % %'
UNION
SELECT DISTINCT ship
FROM Outcomes
WHERE ship LIKE '% % %'