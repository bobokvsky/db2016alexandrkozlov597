SELECT DISTINCT PC1.model, PC2.model, PC1.speed, PC1.RAM
FROM PC AS PC1, PC AS PC2
WHERE (PC1.model > PC2.model) and (PC1.speed = PC2.speed) and (PC1.RAM = PC2.RAM)

SELECT DISTINCT Product.type, Product.model, Laptop.speed
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
WHERE Laptop.speed < (SELECT min(PC.speed) FROM PC)

SELECT DISTINCT Product.maker, Printer.price
FROM Product INNER JOIN Printer ON Product.model = Printer.model
WHERE Printer.price = (SELECT min(Printer.price) FROM 
Printer WHERE Printer.color = 'y') AND Printer.color = 'y'

SELECT DISTINCT Product.maker, AVG(Laptop.screen)
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
GROUP BY Product.maker

SELECT DISTINCT Product.maker, COUNT(Product.model)
FROM Product
WHERE Product.type = 'PC'
GROUP BY maker
HAVING COUNT(DISTINCT Product.model) >= 3

SELECT Product.maker, MAX(PC.price)
FROM Product INNER JOIN PC ON PC.model = Product.model
GROUP BY maker
HAVING (count(*) > 0)

SELECT DISTINCT PC.speed, AVG(PC.price)
FROM PC
WHERE PC.speed > 600
GROUP BY speed

SELECT DISTINCT Product.maker
FROM Product INNER JOIN PC ON Product.model = PC.model
WHERE PC.speed >= 750
INTERSECT
SELECT DISTINCT Product.maker
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
WHERE Laptop.speed >= 750

WITH New_table AS (
  SELECT PC.model, PC.price
  FROM PC
  UNION
  SELECT Laptop.model, Laptop.price
  FROM Laptop
  UNION
  SELECT Printer.model, Printer.price
  FROM Printer
)
SELECT DISTINCT New_table.model
FROM New_table
WHERE New_table.price IN (SELECT MAX(price) FROM New_table)

SELECT DISTINCT maker
FROM Product
WHERE (type = 'Printer') and (maker IN (SELECT maker
FROM Product INNER JOIN PC ON PC.model = Product.model
WHERE ( (PC.ram = (SELECT MIN(ram) FROM PC))) AND PC.speed = (SELECT MAX(speed) FROM PC WHERE PC.ram = (SELECT MIN(ram) FROM PC))) )

WITH Tab AS (
  SELECT PC.price, PC.code, PC.model
  FROM PC INNER JOIN Product ON PC.model = Product.model
  WHERE Product.maker = 'A'
  UNION
  SELECT Laptop.price, Laptop.code, Laptop.model
  FROM Laptop INNER JOIN Product ON Laptop.model = Product.model
  WHERE Product.maker = 'A'
)
SELECT AVG(price)
FROM Tab

SELECT DISTINCT Product.maker, AVG(PC.hd)
FROM Product INNER JOIN PC ON Product.model = PC.model
WHERE Product.maker IN (SELECT Product.maker FROM Product WHERE Product.type = 'Printer')
GROUP BY maker